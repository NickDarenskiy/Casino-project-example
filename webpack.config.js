const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPluginMapping = require('./src/mapping/HtmlWebpackPluginMapping');
const CopyPlugin = require("copy-webpack-plugin");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");
const WebpackIconfontPluginNodejs = require('webpack-iconfont-plugin-nodejs');
const dir = 'src/';

module.exports = {
    mode: 'development',
    entry: {
        master: './src/js/blocks/master/master.js',
        index: './src/js/index.js',
        about: './src/js/pages/about.js',
        terms: './src/js/pages/terms.js',
        login: './src/js/pages/login.js',
        loginCode: './src/js/pages/loginCode.js',
        blocked: './src/js/pages/blocked.js',
        construction: './src/js/pages/blocked.js',
        notAvaliable: './src/js/pages/blocked.js',
        restricted: './src/js/pages/blocked.js',
        403: './src/js/pages/blocked.js',
        505: './src/js/pages/blocked.js',
        faq: './src/js/pages/faq.js',
        404: './src/js/pages/404.js',
        registration: './src/js/pages/registration.js',
        loyalty: './src/js/pages/loyalty.js',
        main: './src/js/pages/main.js',
        games: './src/js/pages/games.js',
        game: './src/js/pages/game.js',
        news: './src/js/pages/news.js',
        newsInfo: './src/js/pages/news-info.js',
        bonusInfo: './src/js/pages/bonus-info.js',
        gamesCategory: './src/js/pages/games.js',
        bonuses: './src/js/pages/bonuses.js',
        bonusesPlayer: './src/js/pages/bonuses-player.js',
        bonusesHistory: './src/js/pages/bonuses-history.js',
        noGames: './src/js/pages/404.js',
        profile: './src/js/pages/profile.js',
        deposits: './src/js/pages/deposits.js',
        withdrawals: './src/js/pages/withdrawals.js',
        messages: './src/js/pages/messages.js',
        documents: './src/js/pages/documents.js',
        accounts: './src/js/pages/accounts.js',
        transactions: './src/js/pages/transactions-history.js',
        messagesInfo: './src/js/pages/messages-info.js',
        profileEdit: './src/js/pages/profile-edit.js',
        auth: './src/js/pages/auth.js',
        auth2: './src/js/pages/auth.js',
        authConfirmation: './src/js/pages/auth-confirmation.js',
        tournaments: './src/js/pages/tournaments.js',
        tournaments_info: './src/js/pages/tournaments-info.js',
        responsible: './src/js/pages/responsible.js',
        form: './src/js/pages/form.js',
        quests: './src/js/pages/quests.js'
    },
    externals: {
        jquery: 'jQuery'
    },
    plugins: [
        new CopyPlugin({
            patterns: [
                {from: "src/img", to: "img"},
                {from: "src", to: "../assets/front"},
                {from: "src/img", to: "../public/resources/img"},
                {from: "src/icons", to: "../public/resources/icons"},
                // {from: "../dist/html/index.html", to: "../dist/index.html"}
            ],
        }),
        new WebpackIconfontPluginNodejs({
            fontName: 'my-icons',
            cssPrefix: 'icon',
            svgs: path.join(dir, 'icons/*.svg'),
            // template: path.join(dir, 'css.njk'),
            fontsOutput: path.join(dir, '/fonts/'),
            cssOutput: path.join(dir, '/css/base/font.css'),
            htmlOutput: path.join(dir, '/html/components/icons.html'),
            jsOutput: false,
            namesOutput: false,
            formats: ['woff2'],
        }),
        new BrowserSyncPlugin({
            // browse to http://localhost:3000/ during development,
            // ./public directory is being served
            host: 'localhost',
            port: 3000,
            server: {
                baseDir: ['../dist'],
                index: "html/index.html"
            }
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        })
    ].concat(HtmlWebpackPluginMapping),
    output: {
        filename: 'bundles/[name].bundle.js',
        path: path.resolve(__dirname, '../dist'),
        clean: true,
    },
    /* optimization: {
       splitChunks: {
         chunks: 'all',
       },
     },*/
    module: {
        rules: [
            {
                test: /\.twig$/,
                use: [
                    'raw-loader',
                    {
                        loader: 'twig-html-loader',
                        options: {
                            namespaces: {
                                'master': path.join(__dirname, '/src/html/master'),
                                'components': path.join(__dirname, '/src/html/components'),
                                'fragments': path.join(__dirname, '/src/html/fragments'),
                                'modules': path.join(__dirname, '/src/html/modules'),
                            },
                            data: (context) => {
                                const data = path.join(__dirname, '/src/resources/data.json');
                                context.addDependency(data); // Force webpack to watch file
                                return context.fs.readJsonSync(data, {throws: false}) || {};
                            }
                        }
                    }
                ]
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    "style-loader",
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: true,
                        },
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true,
                        },
                    },
                ],
            },
            { // Images resources
                test: /\.(png|svg|jpg|jpeg|gif|webp)$/i,
                type: 'asset/resource',
            },
            { // Fonts
                test: /\.(woff|woff2|eot|ttf|otf)$/i,
                type: 'asset/resource',
            },
        ],
    },
};
