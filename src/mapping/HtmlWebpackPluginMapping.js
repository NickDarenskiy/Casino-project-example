const HtmlWebpackPlugin = require('html-webpack-plugin');

const HtmlWebpackPluginMapping = [
  new HtmlWebpackPlugin({
    chunks: ['index'],
    template: './src/html/index.twig',
    filename: 'html/index.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master'],
    template: './src/html/components/icons.html',
    filename: 'html/icons.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master'],
    template: './src/html/pages/components.twig',
    filename: 'html/pages/components.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'about'],
    template: './src/html/pages/about.twig',
    filename: 'html/pages/about.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'terms'],
    template: './src/html/pages/terms.twig',
    filename: 'html/pages/terms.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'login'],
    template: './src/html/pages/login.twig',
    filename: 'html/pages/login.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'loginCode'],
    template: './src/html/pages/login_code.twig',
    filename: 'html/pages/login_code.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'help'],
    template: './src/html/pages/help.twig',
    filename: 'html/pages/help.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master'],
    template: './src/html/pages/search.twig',
    filename: 'html/pages/search.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master'],
    template: './src/html/pages/toaster.twig',
    filename: 'html/pages/toaster.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'bonusInfo'],
    template: './src/html/pages/bonus-info.twig',
    filename: 'html/pages/bonus-info.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['blocked'],
    template: './src/html/pages/blocked.twig',
    filename: 'html/pages/blocked.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['construction'],
    template: './src/html/pages/construction.twig',
    filename: 'html/pages/construction.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['notAvaliable'],
    template: './src/html/pages/not-avaliable.twig',
    filename: 'html/pages/not-avaliable.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['restricted'],
    template: './src/html/pages/restricted.twig',
    filename: 'html/pages/restricted.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['403'],
    template: './src/html/pages/403.twig',
    filename: 'html/pages/403.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['restricted'],
    template: './src/html/pages/500.twig',
    filename: 'html/pages/500.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'faq'],
    template: './src/html/pages/faq.twig',
    filename: 'html/pages/faq.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'blocked', '404'],
    template: './src/html/pages/404.twig',
    filename: 'html/pages/404.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'registration'],
    template: './src/html/pages/registration.twig',
    filename: 'html/pages/registration.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'update_data'],
    template: './src/html/pages/update_data.twig',
    filename: 'html/pages/update_data.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'profileEdit'],
    template: './src/html/pages/profile-edit.twig',
    filename: 'html/pages/profile-edit.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'loyalty'],
    template: './src/html/pages/loyalty.twig',
    filename: 'html/pages/loyalty.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'main'],
    template: './src/html/pages/main.twig',
    filename: 'html/pages/main.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master'],
    template: './src/html/pages/loader.twig',
    filename: 'html/pages/loader.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'news'],
    template: './src/html/pages/news.twig',
    filename: 'html/pages/news.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'newsInfo'],
    template: './src/html/pages/news-info.twig',
    filename: 'html/pages/news-info.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'main'],
    template: './src/html/pages/main-logged.twig',
    filename: 'html/pages/main-logged.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'login'],
    template: './src/html/pages/password_recovery.twig',
    filename: 'html/pages/password_recovery.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'login'],
    template: './src/html/pages/password_verification.twig',
    filename: 'html/pages/password_verification.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'login'],
    template: './src/html/pages/password_new.twig',
    filename: 'html/pages/password_new.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'games'],
    template: './src/html/pages/games.twig',
    filename: 'html/pages/games.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'game'],
    template: './src/html/pages/game.twig',
    filename: 'html/pages/game.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'gamesCategory'],
    template: './src/html/pages/games-category.twig',
    filename: 'html/pages/games-category.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'bonuses'],
    template: './src/html/pages/bonuses.twig',
    filename: 'html/pages/bonuses.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'bonusesPlayer'],
    template: './src/html/pages/bonuses-player.twig',
    filename: 'html/pages/bonuses-player.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'bonusesHistory'],
    template: './src/html/pages/bonuses-history.twig',
    filename: 'html/pages/bonuses-history.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'gamesCategory'],
    template: './src/html/pages/games-favorite.twig',
    filename: 'html/pages/games-favorite.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'login'],
    template: './src/html/pages/confirm-email.twig',
    filename: 'html/pages/confirm-email.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'profile'],
    template: './src/html/pages/profile.twig',
    filename: 'html/pages/profile.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'deposits'],
    template: './src/html/pages/deposits.twig',
    filename: 'html/pages/deposits.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'withdrawals'],
    template: './src/html/pages/withdrawals.twig',
    filename: 'html/pages/withdrawals.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'messages'],
    template: './src/html/pages/messages.twig',
    filename: 'html/pages/messages.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'transactions'],
    template: './src/html/pages/transactions-history.twig',
    filename: 'html/pages/transactions-history.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'documents'],
    template: './src/html/pages/documents.twig',
    filename: 'html/pages/documents.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'accounts'],
    template: './src/html/pages/accounts.twig',
    filename: 'html/pages/accounts.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'messagesInfo'],
    template: './src/html/pages/messages-info.twig',
    filename: 'html/pages/messages-info.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'blocked', '404'],
    template: './src/html/pages/no-games.twig',
    filename: 'html/pages/no-games.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'auth'],
    template: './src/html/pages/auth.twig',
    filename: 'html/pages/auth.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'auth'],
    template: './src/html/pages/auth2.twig',
    filename: 'html/pages/auth2.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'authConfirmation'],
    template: './src/html/pages/2fac-confirmation-code.twig',
    filename: 'html/pages/2fac-confirmation-code.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'authConfirmation'],
    template: './src/html/pages/2fac-backup-key.twig',
    filename: 'html/pages/2fac-backup-key.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'authConfirmation'],
    template: './src/html/pages/2fac-save-backup-key.twig',
    filename: 'html/pages/2fac-save-backup-key.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'tournaments'],
    template: './src/html/pages/tournaments-list.twig',
    filename: 'html/pages/tournaments-list.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'tournaments_info'],
    template: './src/html/pages/tournaments-info.twig',
    filename: 'html/pages/tournaments-info.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'responsible'],
    template: './src/html/pages/responsible.twig',
    filename: 'html/pages/responsible.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master', 'quests'],
    template: './src/html/pages/quests.twig',
    filename: 'html/pages/quests.html'
  }),

  // modals

  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/preactivation.twig',
    filename: 'html/modals/preactivation.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/share-my-win.twig',
    filename: 'html/modals/share-my-win.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/booster-wins.twig',
    filename: 'html/modals/booster-wins.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/new-limit.twig',
    filename: 'html/modals/new-limit.html'
  }),

  // modals

  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/preactivation.twig',
    filename: 'html/modals/preactivation.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/share-my-win.twig',
    filename: 'html/modals/share-my-win.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/booster-wins.twig',
    filename: 'html/modals/booster-wins.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/new-limit.twig',
    filename: 'html/modals/new-limit.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/loader.twig',
    filename: 'html/modals/loader.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/success.twig',
    filename: 'html/modals/success.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/sign-in.twig',
    filename: 'html/modals/sign-in.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/sign-up.twig',
    filename: 'html/modals/sign-up.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master'],
    template: './src/html/pages/modals.twig',
    filename: 'html/pages/modals.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/test.twig',
    filename: 'html/modals/test.html'
  }),

  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/template.twig',
    filename: 'html/modals/template.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/fiat.twig',
    filename: 'html/modals/fiat.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/tournaments-games.twig',
    filename: 'html/modals/tournaments-games.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/tournaments-text.twig',
    filename: 'html/modals/tournaments-text.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/tournaments-list.twig',
    filename: 'html/modals/tournaments-list.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/confirmation-phone-mail.twig',
    filename: 'html/modals/confirmation-phone-mail.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/verification-2.twig',
    filename: 'html/modals/verification-2.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/verification.twig',
    filename: 'html/modals/verification.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/warning-2.twig',
    filename: 'html/modals/warning-2.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/warning.twig',
    filename: 'html/modals/warning.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/check-extensions.twig',
    filename: 'html/modals/check-extensions.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/additional-data.twig',
    filename: 'html/modals/additional-data.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/bonuses-default.twig',
    filename: 'html/modals/bonuses-default.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/deposit-bonuses.twig',
    filename: 'html/modals/deposit-bonuses.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/error.twig',
    filename: 'html/modals/error.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/deposit-metamask.twig',
    filename: 'html/modals/deposit-metamask.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/deposit-crypto-wallets.twig',
    filename: 'html/modals/deposit-crypto-wallets.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/deposit-default.twig',
    filename: 'html/modals/deposit-default.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/deposit-qr.twig',
    filename: 'html/modals/deposit-qr.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/cash-withdrawal-2-notification.twig',
    filename: 'html/modals/cash-withdrawal-2-notification.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/cash-withdrawal-2-form.twig',
    filename: 'html/modals/cash-withdrawal-2-form.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/cash-withdrawal-form.twig',
    filename: 'html/modals/cash-withdrawal-form.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/cash-withdrawal-notification-2.twig',
    filename: 'html/modals/cash-withdrawal-notification-2.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/cash-withdrawal-notification-1.twig',
    filename: 'html/modals/cash-withdrawal-notification-1.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/cash-withdrawal.twig',
    filename: 'html/modals/cash-withdrawal.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/success.twig',
    filename: 'html/modals/success.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/profile-filling.twig',
    filename: 'html/modals/profile-filling.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/new-account.twig',
    filename: 'html/modals/new-account.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/new-account-select.twig',
    filename: 'html/modals/new-account-select.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/save.twig',
    filename: 'html/modals/save.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/file.twig',
    filename: 'html/modals/file.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/logout.twig',
    filename: 'html/modals/logout.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/login.twig',
    filename: 'html/modals/login.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['modals'],
    template: './src/html/modals/trustwallet.twig',
    filename: 'html/modals/trustwallet.html'
  }),
  new HtmlWebpackPlugin({
    chunks: ['master'],
    template: './src/html/pages/modals.twig',
    filename: 'html/pages/modals.html'
  })
];

module.exports = HtmlWebpackPluginMapping;
