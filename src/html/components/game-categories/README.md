### The "game categories" block has 3 types
- centered-navigation: slider navigation on the sides of the container
- header-navigation
- scroll

### To delete an unnecessary type, you must:
- [ ] Delete template type from [html/.../game-categories](/dev/src/html/components/game-categories/) directory
- [ ] Delete twig include of unnecessary template types from [components/main.twig](/dev/src/html/components/main.twig)
- [ ] Delete styles of template type from [css/.../game-categories](/dev/src/css/components/game-categories/)
- [ ] Delete style imports of unnecessary template types from the top of the [game-categories/index.scss](/dev/src/css/components/game-categories/index.scss)