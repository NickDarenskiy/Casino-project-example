## The main slider has several options that are set via data attributes

- `data-main-swiper`: to be determined by javascript
- `data-slides-per-view`: determination of the number of slides at all resolutions
- `data-desktop-slides-per-view`: determination of the number of slides from a resolution of 1280px. [Example](./two-slides.twig)

The [script](/dev/src/js/blocks/components/swiper-main.js) can handle multiple sliders
