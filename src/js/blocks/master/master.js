import '../../../css/index.scss';

import '../components/font-ready';
import '../components/sidebar-open';
import '../components/sidebar-menu';
import '../components/user-info';
import '../components/footer-menu';
import '../components/currency';
import '../components/tabs';
import '../components/modals-inline';
import '../components/tooltips';
import '../components/modals';
import '../components/accordion';
import '../components/cut-text';

import '../components/swiper-providers';
import '../components/swiper-payments';

$(document).ready(function () {
  $('[data-preloader-full-front]').remove();
});

$(document).ready(function () {
  // Here we store useful scripts for layout. Like preloader, etc...
  /** Глобальные переменные */
  const WIN = $(window),
    DOC = $(document),
    HTML = $('html'),
    BODY = $('body');
});
