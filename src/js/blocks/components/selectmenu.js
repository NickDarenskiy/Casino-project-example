$(document).ready(function () {
    function selectInit() {
        $('[data-select-menu]').each(function () {
            if (!$(this).parent().hasClass('selectric-hide-select')) {
                $(this).selectric({
                    maxHeight: 200,
                    disableOnMobile: false,
                    nativeOnMobile: false,
                    arrowButtonMarkup: '<b class="selectric__button icon-arrow-down"></b>',
                    onInit: function () {
                        var current_option = $(this),
                            current_option_value = current_option.val(),
                            selectric_container = $(this).parents('.selectric-wrapper').find('.selectric'),
	                        selectric_label = selectric_container.find(".label"),
	                        data_static_placeholder = $(
		                        "select[data-static-placeholder]"
	                        ).data("static-placeholder");

                        // проверка для вывода поля с секретным ответом
                        if (typeof current_option.attr('data-secret-question') !== typeof undefined && current_option.attr('data-secret-question') !== false) {
                            if (current_option_value) {
                                secret_answer.show();
                            } else {
                                secret_answer.hide();
                            }
                        }

                        //  эмуляция placeholder'a для селектрика
                        if (current_option_value) {
                            selectric_container.removeClass('selectric--placeholder');
                        } else {
                            selectric_container.addClass('selectric--placeholder');
                        }

                        // проверка на количество эллементов, если 1 - то добавляем класс,
                        // который скрывает всю навигацию
                        if (current_option.find('option').length === 1) {
                            current_option.parents('.selectric-wrapper').addClass('selectric--void');
                        }

	                    //подменяем значение label значением атрибута data-static-placeholder для multiple

	                    if (current_option.attr("multiple")) {
		                    selectric_label.text(data_static_placeholder);
	                    }

	                    if (current_option_value.length !== 0) {
		                    $(".categories__inner--main .icon-providers").addClass(
			                    "filter-applied"
		                    );
	                    } else {
		                    $(".categories__inner--main .icon-providers").removeClass(
			                    "filter-applied"
		                    );
	                    }
                    },
                    onChange: function (element) {
                        var current_option = $(this),
                            current_option_value = current_option.val(),
                            selectric_container = $(this).parents('.selectric-wrapper').find('.selectric'),
	                        selectric_label = selectric_container.find(".label"),
		                    data_static_placeholder = $(
			                    "select[data-static-placeholder]"
		                    ).data("static-placeholder");

                        // проверка для вывода поля с секретным ответом
                        if (typeof current_option.attr('data-secret-question') !== typeof undefined && current_option.attr('data-secret-question') !== false) {
                            if (current_option_value) {
                                secret_answer.show();
                            } else {
                                secret_answer.hide();
                            }
                        }

                        //  эмуляция placeholder'a для селектрика
                        if (current_option_value) {
                            selectric_container.removeClass('selectric--placeholder');
                        } else {
                            selectric_container.addClass('selectric--placeholder');
                        }

	                    //подменяем значение label значением атрибута data-static-placeholder для multiple
	                    // if (current_option.attr("multiple")) {
		                //     selectric_label.text(data_static_placeholder);
	                    // }
	                    if (current_option_value.length !== 0) {
		                    $(".categories__inner--main .icon-providers").addClass(
			                    "filter-applied"
		                    );
	                    } else {
		                    $(".categories__inner--main .icon-providers").removeClass(
			                    "filter-applied"
		                    );
	                    }
                    },
                    onOpen: function (element) {
                        const closestSelect = element.closest('.select');

                        if (closestSelect) {
                            closestSelect.style.zIndex = '999999';
                        }
                    },
                    onClose: function (element) {
                        const closestSelect = element.closest('.select');

                        if (closestSelect) {
                            closestSelect.style.zIndex = '1';
                        }
                    },
	                optionsItemBuilder: function (itemData, element, index) {
		                const isMultiple = element[0].multiple;
		                if (!isMultiple) return itemData.text;
		                const { count } = itemData.element[0].dataset;
		                return `<label class="checkbox-label">
			                      <input type="checkbox" class="checkbox-input">
			                      <span class="checkbox-box"></span>
			                      <span class="checkbox-text">${itemData.text}</span>
			                      ${
						                count
							                ? `<span class="checkbox-count">${count}</span>`
							                : ""
					                }
			                  </label>`;
	                },
                });
            }
        });
    }

    selectInit();

    $(document).on('ajaxSuccess', function () {
        selectInit();
    });
});

