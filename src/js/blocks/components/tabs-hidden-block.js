$(document).ready(function() {
    $('.tab ul.tabs li').on('click', function () {
        if ($(this).hasClass('tabs__visible-trigger')) {
            $('body').find('.tabs-hidden-block').addClass('open');
        } else {
            $('body').find('.tabs-hidden-block').removeClass('open');
        }
    });

});
