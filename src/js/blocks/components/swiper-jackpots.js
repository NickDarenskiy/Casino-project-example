import Swiper from '../vendor/swiper-bundle.min.js';

$(document).ready(function () {
    let jackpotsOneProviders = new Swiper('[data-jackpots-swiper]', {
        slidesPerView: 4,
        spaceBetween: 16,
        navigation: {
            nextEl: ".jackpots__header .swiper-button-next",
            prevEl: ".jackpots__header .swiper-button-prev",
        },
        breakpoints: {
            640: {
                slidesPerView: 2,
                spaceBetween: 24
            },
            1281: {
                slidesPerView: 2,
                spaceBetween: 24
            }
        }
    });

});