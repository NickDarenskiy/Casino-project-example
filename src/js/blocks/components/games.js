const colculateMultiplesGame = () => {
  const gamesList = $('[data-games-list]');

  if (gamesList.length === 0) {
    return;
  }

  gamesList.each((index, game) => {
    const gameCards = $(game).find('.game-card');

    gameCards.each((index, card) => {
      if ((index + 1) % 10 === 0 && index + 1 >= 20) {
        const gridRow = Math.floor(index / 10) * 4 + 3;
        $(card).css('--grid-row', gridRow);
      }
    });
  });
};

$('body').on('game-batch-added', () => {
  colculateMultiplesGame();
});
