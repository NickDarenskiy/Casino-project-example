import Swiper from '../vendor/swiper-bundle.min.js';

$(document).ready(function () {
    var swiperProviders = new Swiper("[data-payments-swiper]", {
        slidesPerView: 5,
        spaceBetween: 16,
        navigation: {
            nextEl: ".payments .swiper-button-next",
            prevEl: ".payments .swiper-button-prev",
        },
        breakpoints: {
            280: {
                slidesPerView: 2
            },
            481: {
                slidesPerView: 3
            },
            641: {
                slidesPerView: 4,
                spaceBetween: 24
            },
            1281: {
                slidesPerView: 6,
                spaceBetween: 24
            }
        }
    });


});