import "../vendor/jquery.fullscreen.min";

$(document).on("click", "[data-game-widescreen]", function () {
    $(this).parents('.game').toggleClass('game--theatre');
});

$(document).on('click', '[data-game-fullscreen]', function () {
    $('.game__iframe iframe, .game__iframe img').fullscreen();
});