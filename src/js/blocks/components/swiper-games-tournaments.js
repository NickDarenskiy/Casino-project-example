import Swiper from '../vendor/swiper-bundle.min.js';

$(document).ready(function () {
  $('.icon-favorite').click(function () {
    $(this).toggleClass('active');
  });

  new Swiper("[data-games-tournament-swiper]", {
    slidesPerView: 2,
    spaceBetween: 8,
    navigation: {
      nextEl: ".games--tournaments .swiper-button-next",
      prevEl: ".games--tournaments .swiper-button-prev",
    },
    breakpoints: {
      320: {
        slidesPerView: 2,
        spaceBetween: 8
      },
      768: {
        slidesPerView: 4,
        spaceBetween: 16
      },
      1280: {
        slidesPerView: 4,
        spaceBetween: 24
      },
      1920: {
        slidesPerView: 5,
        spaceBetween: 24,
      }
    }
  });
});
