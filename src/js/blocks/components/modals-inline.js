import '../vendor/jquery.magnific-popup';

$(document).ready(function() {
    // Inline popups
    $('[data-sidebar-external]').magnificPopup({
        removalDelay: 200, //delay removal by X to allow out-animation
        callbacks: {
            beforeOpen: function() {
                this.st.mainClass = this.st.el.attr('data-effect');
            }
        },
        midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });
});