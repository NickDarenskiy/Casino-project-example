$(document).ready(function () {
  var shortText= $('[data-cut-text]');
  shortText.each(function(){
    var size = $(this).data('cutText'),
      elementText = $(this).text();
    if(elementText.length > size){
      $(this).text(elementText.slice(0, size) + '...');
    }
  });
});
