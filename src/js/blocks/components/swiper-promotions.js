import Swiper from '../vendor/swiper-bundle.min.js';

$(document).ready(function () {
    var swiperPromotions = new Swiper("[data-main-swiper]", {
        slidesPerView: 2,
        // loop: true,
        spaceBetween: 16,
        breakpoints: {
            280: {
                slidesPerView: 1
            },
            769: {
                slidesPerView: 2,
                spaceBetween: 24
            },
            1281: {
                slidesPerView: 3,
                spaceBetween: 24
            }
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
        }
    });
});