import Swiper from "../vendor/swiper-bundle.min.js";

$(document).ready(function () {
  var swiperLoyalty = new Swiper("[data-loyalty-swiper]", {
    slidesPerView: 2,
    spaceBetween: 16,
    navigation: {
      nextEl: ".loyalty .swiper-button-next",
      prevEl: ".loyalty .swiper-button-prev",
    },
    breakpoints: {
      280: {
        slidesPerView: 1,
      },
      640: {
        slidesPerView: 2,
        spaceBetween: 24,
      },
    },
  });
});
