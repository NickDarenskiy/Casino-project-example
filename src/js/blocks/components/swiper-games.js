import Swiper from '../vendor/swiper-bundle.min.js';

$(document).ready(function () {
    $('.icon-favorite').click(function () {
        $(this).toggleClass('active');
    });
    let gamesOneProviders = new Swiper(".games--1 .games__list", {
        slidesPerView: 5,
        spaceBetween: 16,
        navigation: {
            nextEl: ".games--1 .swiper-button-next",
            prevEl: ".games--1 .swiper-button-prev",
        },
        breakpoints: {
            280: {
                slidesPerView: 2
            },
            481: {
                slidesPerView: 3
            },
            640: {
                slidesPerView: 4,
                spaceBetween: 24
            },
            1281: {
                slidesPerView: 5,
                spaceBetween: 24
            },
            1561: {
                slidesPerView: 6,
                spaceBetween: 24
            }
        }
    });
    let gamesTwoProviders = new Swiper(".games--2 .games__list", {
        slidesPerView: 5,
        spaceBetween: 16,
        navigation: {
            nextEl: ".games--2 .swiper-button-next",
            prevEl: ".games--2 .swiper-button-prev",
        },
        breakpoints: {
            280: {
                slidesPerView: 2
            },
            481: {
                slidesPerView: 3
            },
            640: {
                slidesPerView: 4,
                spaceBetween: 24
            },
            1281: {
                slidesPerView: 5,
                spaceBetween: 24
            },
            1561: {
                slidesPerView: 6,
                spaceBetween: 24
            }
        }
    });
    let gamesThreeProviders = new Swiper(".games--3 .games__list", {
        slidesPerView: 5,
        spaceBetween: 16,
        navigation: {
            nextEl: ".games--3 .swiper-button-next",
            prevEl: ".games--3 .swiper-button-prev",
        },
        breakpoints: {
            280: {
                slidesPerView: 2
            },
            481: {
                slidesPerView: 3
            },
            640: {
                slidesPerView: 4,
                spaceBetween: 24
            },
            1281: {
                slidesPerView: 5,
                spaceBetween: 24
            },
            1561: {
                slidesPerView: 6,
                spaceBetween: 24
            }
        }
    });
    let gamesFourProviders = new Swiper(".games--4 .games__list", {
        slidesPerView: 5,
        spaceBetween: 16,
        navigation: {
            nextEl: ".games--4 .swiper-button-next",
            prevEl: ".games--4 .swiper-button-prev",
        },
        breakpoints: {
            280: {
                slidesPerView: 2
            },
            481: {
                slidesPerView: 3
            },
            640: {
                slidesPerView: 4,
                spaceBetween: 24
            },
            1281: {
                slidesPerView: 5,
                spaceBetween: 24
            },
            1561: {
                slidesPerView: 6,
                spaceBetween: 24
            }
        }
    });
});