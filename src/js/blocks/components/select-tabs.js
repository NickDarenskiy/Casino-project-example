/**
 *  Табы в виде трансформера из табов в селекты
 *
 *    data-tabs - аттрибут, по которому происходит инициализация работы табов
 *
 *    data-tabs-save - аттрибут, который, фактически активирует сохранение активного таба в LocalStorage
 *        Для него нужно указать имя, которое будет использоваться для идентификации текущей страницы
 *
 *    data-tabs-sync - аттрибут, который активирует синхронизацию двух табов
 *        Его нужно задать для того селекта, С КОТОРЫМ нужно синхронизировать активный
 *        Если задать для обоих селектов - синхронизация будет работать в обе стороны
 *
 *    data-tabs-transform - аттрибут, значение в котороым указывает ширину экрана, меньше которой табы будут трансформироваться в селект
 *
 *    data-tabs-selector - аттрибут, добавляем его в случае, если нужно сделать выделение таба, которое с анимацией
 *        передвигается после выделения таба
 *
 * */

function selectTabs() {
    let tabs_active = 'active',
        tabs_content = '.tabs__content';
    $('[data-tabs]').each(function (idx, elem) {
        const selectricInited = $(this).parent().hasClass('selectric-hide-select')
        if (false === selectricInited) {
            $(this).selectric({
                maxHeight: 200,
                disableOnMobile: false,
                nativeOnMobile: false,
                responsive: false,
                arrowButtonMarkup: '<b class="selectric__button icon-arrow-down"></b>',
                onOpen: function (element) {
                    const closestSelect = element.closest('.select');

                    if (closestSelect) {
                        closestSelect.style.zIndex = '999999';
                    }
                },
                onClose: function (element) {
                    const closestSelect = element.closest('.select');

                    if (closestSelect) {
                        closestSelect.style.zIndex = '1';
                    }
                }
            })
                .on('init', function () {
                    $('body').trigger('selectric-init');
                })
                .on('change', function () {
                    let tabs_parent = $(this).closest('.tabs__parent'),
                        tabs_save = $(this).closest('[data-tabs-save]'),
                        tabs_save_value = tabs_save.data('tabs-save'),
                        tab_counter = $(this).parents('.selectric-wrapper').find('li.selected').index();

                    tabs_parent.find(tabs_content + ':first').parent().children(tabs_content).removeClass(tabs_active).eq(tab_counter).addClass(tabs_active);
                    // вызов отрисовки слайдера джекпотов

                    // если указан дата аттрибут для сохранения - сохраняем его в localStorage
                    if (tabs_save.length && tabs_save_value.length) {
                        localStorage.setItem(tabs_save_value, tab_counter);
                    }

                    // синхронизация двух селектов
                    // в случае сложной адаптивной структуры, если по дизайну селект (табы) переносятся в другой блок
                    $('[data-tabs-sync]').each(function () {
                        $(this).parents('.selectric-wrapper').find('li').removeClass('selected').eq(tab_counter).addClass('selected').trigger('click');
                    })
                });

            // находим общего родителя для табов и контена табов и проставляем классы
            let tabs_parent = $($(this).parents().get().find(itm => $(itm).find(tabs_content).length)),
                tabs_save = $(this).closest('[data-tabs-save]'),
                tabs_save_value = tabs_save.data('tabs-save');

            tabs_parent.addClass('tabs__parent');

            // если установлено сохранение активного таба - при загрузке страницы эмулируем клик на него
            if (tabs_save.length && tabs_save_value.length) {
                setTimeout(function () {

                    // если локал сторейдж пустой то открываем там с классом tabs-show-first или если класс не навешен то открываем первый таб
                    let indexActiveTab = localStorage.getItem(tabs_save_value)
                        ? localStorage.getItem(tabs_save_value)
                        : $(elem).closest('.selectric-wrapper').find('.selectric-items .tabs-show-first').data('index') || 0

                    // если не будет установлено ни одного active у контента, то добавляем его сами
                    tabs_parent.find(tabs_content).eq(indexActiveTab).addClass(tabs_active);

                    tabs_save.parents('.selectric-wrapper').find('li:eq(' + indexActiveTab + ')').trigger('click');
                }, 1);
            }
        }
    });
}

$(document).ready(function () {
    selectTabs();
})

$(document).on('ajaxSuccess', function () {
    selectTabs();
});

/** End New tabs **/
