$(document).ready(function () {
  // --- menu open + close stuff --------------
  let menuTitle = $('.footer-menu__title');
  let collapsedMenu = $('.footer-menu__list');
  function rolledMenu() {
    $(this).toggleClass('closed').siblings(collapsedMenu).slideToggle(200);
  }
  function removeClickHandler() {
    $(menuTitle).off('click', rolledMenu);
  }
  function addClickHandler() {
    $(menuTitle).on('click', rolledMenu);
  }

  let mediaQuery = window.matchMedia('screen and (max-width: 640px)');
  mediaQuery.addListener(foo);
  foo(mediaQuery);
  function foo(mq) {
    if (mq.matches) {
      $(menuTitle).addClass('closed');
      $(collapsedMenu).css('display', 'none');
      addClickHandler();
    } else {
      removeClickHandler();
      $(menuTitle).removeClass('closed');
      $(collapsedMenu).css('display', '');
    }
  }
  // ---END menu open + close stuff --------------
});
