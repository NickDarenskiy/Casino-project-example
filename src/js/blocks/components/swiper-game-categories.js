import Swiper from '../vendor/swiper-bundle.min.js';

const swiperOptions = {
    'header-navigation': {
        slidesPerView: 1,
        spaceBetween: 10,
        breakpoints: {
            768: {
                slidesPerView: 3,
            },
            1280: {
                slidesPerView: 4,
            },
            1920: {
                slidesPerView: 6,
            }
        }
    },
}

function initGameCatgegoriesSwiper(node) {
    const swiper = node.querySelector('.swiper-container');
    const sliderType = node.dataset.swiperGameCategories || Object.keys(swiperOptions)[0];
    const copiedSwiperOptions = swiperOptions[sliderType] || swiperOptions[Object.keys(swiperOptions)[0]];
    const navigationNextEl = node.querySelector('.swiper-button-next');
    const navigationPrevEl = node.querySelector('.swiper-button-prev');

    new Swiper(swiper, {
        navigation: {
            nextEl: navigationNextEl,
            prevEl: navigationPrevEl,
        },
        ...copiedSwiperOptions
    })
}

$(document).ready(function () {
    const swiperSelector = '[data-swiper-game-categories]';
    const swiperNodes = document.querySelectorAll(swiperSelector);

    if (!swiperNodes) return;
    swiperNodes.forEach(initGameCatgegoriesSwiper);
});
