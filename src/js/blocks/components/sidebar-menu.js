$(document).on("click", "[data-sidebar-submenu]", function () {
  $(this).toggleClass("open");
  this.scrollIntoView();

  //костыль для решения скрытия элементов сайдбара под меню на ios планшетах в браузере safari при открытие категории игр
  var sidebar = $("aside.sidebar");
  sidebar.toggleClass("open-submenu");
});
