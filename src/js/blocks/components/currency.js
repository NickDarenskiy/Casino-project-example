$(document).ready(function () {
    const DOC = $(document)
    const currency = $('[data-currency]')
    /** Currency select **/
    currency.on('click', function (){
        const currencyWrapper = $(this).siblings('.currency-select')
        currencyWrapper.toggleClass('visible');
        currency.toggleClass('active');
    });
    /** End Currency select **/
    DOC.on('click', function(e) {
        const currencyWrapper = $('.currency-select')
        if (currencyWrapper.is(':visible')) {
            if (!$(e.target).closest('[data-currency], .currency-select').length) {
                currencyWrapper.removeClass('visible');
                currency.removeClass('active');
            }
            e.stopPropagation();
        }
    });
});