import '../vendor/jquery.magnific-popup'
import '../vendor/jquery.selectric.min'
import '../components/selectmenu'
import '../components/datepicker'

$(document).ready(function () {
    $('[data-modal]').each(function () {
        const isDisabledBgCloseClick = $(this).data('disable-bg-close') || false
        if (!$(this).hasClass('init-modal')) {
            $(this).addClass('init-modal')
            $(this).magnificPopup({
                type: 'ajax',
                removalDelay: 300,
                preloader: true,
                closeOnBgClick: !isDisabledBgCloseClick,
                tLoading: '',
                mainClass: 'mfp-zoom-in',
                ajax: {
                    settings: {
                        error: function (response) {
                            $.magnificPopup.close()
                            setTimeout(() => {
                                $.magnificPopup.open({
                                    items: {
                                        src:
                                            "<div class='wrapping'>" +
                                            response.responseText +
                                            '</div>',
                                        type: 'inline'
                                    }
                                })
                            }, 500)
                        }
                    }
                },
                callbacks: {
                    parseAjax: function (mfpResponse) {
                        mfpResponse.data =
                            "<div class='wrapping'>" +
                            mfpResponse.data +
                            '</div>'
                    },
                    ajaxContentAdded: function () {
                        this.content
                        $('body').trigger('modal-opened', this)
                    },
                    close: function () {
                        $('body').trigger('modal-closed')
                    }
                }
            })
        }
    })
})

$(document).on('ajaxSuccess', function () {
    $('.modal__accordion-header').click(function () {
        $(this).parent().toggleClass('open')
    })
    $('.show-more').click(function () {
        $(this).prev().toggleClass('open')
        $(this).hide()
    })
})
