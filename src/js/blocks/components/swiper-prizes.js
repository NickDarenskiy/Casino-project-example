import Swiper from '../vendor/swiper-bundle.min.js';

$(document).ready(function () {
    var swiperPrizes = new Swiper("[data-prizes-swiper]", {
        slidesPerView: 3,
        spaceBetween: 16,
        navigation: {
            nextEl: ".tournaments--prizes .swiper-button-next",
            prevEl: ".tournaments--prizes .swiper-button-prev",
        },
        breakpoints: {
            280: {
                slidesPerView: 1
            },
            550: {
                slidesPerView: 2
            },
            1090: {
                slidesPerView: 3,
                spaceBetween: 24
            },
            1281: {
                slidesPerView: 3,
                spaceBetween: 24
            }
        }
    });
});