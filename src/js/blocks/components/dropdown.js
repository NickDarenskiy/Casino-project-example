$(document).ready(function () {
    $(document).on("click", ".icon-dots", function () {
        $(this).parent().toggleClass('active');
    });

    $(document).on('click', function (event) {
        const $target = $(event.target);
        if ($target.is('a') || !$target.closest('.dropdown').length) {
            $('.dropdown').removeClass('active');
            if ($target.is('a')) event.preventDefault();
        }
    });
});