import intlTelInput from '../vendor/intlTelInput';

function findElements() {
    const elements = document.querySelectorAll('input[type=tel]');

    return { elements };
}

function initLibrary(input) {
    intlTelInput(input, {
        utilsScript:
            'https://cdn.jsdelivr.net/npm/intl-tel-input@18.1.1/build/js/utils.js',
        preferredCountries: [],
        nationalMode: false,
    });
}

function init() {
    const { elements } = findElements();

    if (elements) {
        [...elements].forEach(initLibrary);
    }
}

init();

$(document).on('ajaxSuccess', function () {
    init();
});
