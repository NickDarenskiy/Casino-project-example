import Swiper from '../vendor/swiper-bundle.min.js';

$(document).ready(function () {
    var swiperTournaments = new Swiper("[data-tournaments-swiper]", {
        slidesPerView: 3,
        spaceBetween: 16,
        navigation: {
            nextEl: ".tournaments .swiper-button-next",
            prevEl: ".tournaments .swiper-button-prev",
        },
        breakpoints: {
            280: {
                slidesPerView: 1
            },
            550: {
                slidesPerView: 2
            },
            1280: {
                slidesPerView: 3,
                spaceBetween: 24
            }
        }
    });
});