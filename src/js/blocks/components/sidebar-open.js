$(document).ready(function () {
    const DOC = $(document)
    /** sidebar toggler for open/close **/

    /** начальное положения страницы **/
  let savedScrollTop = 0;

    DOC.on('click', '[data-sidebar-closer]:not(.opened)', function(event) {
        event.preventDefault();

    /** сохраняем текущее положение страницы */
    if ($(this).is("[data-sidebar-closer-mobile]")) {
        savedScrollTop = $(window).scrollTop();
        $("body").css("top", -savedScrollTop + "px");
      }
      /** **/

        $('.body').addClass('opened');
        $('[data-sidebar]').addClass('opened');
        $('[data-sidebar-closer] i').removeClass('icon-menu-open').addClass('icon-menu-close');
        $('[data-sidebar-closer]').addClass('opened');

    });
    DOC.on('click', '[data-sidebar-closer].opened', function() {
        $('.body').removeClass('opened');
        $('[data-sidebar-closer]').removeClass('opened');
        $('[data-sidebar]').removeClass('opened');
        $('[data-sidebar-closer] i').removeClass('icon-menu-close').addClass('icon-menu-open');

    /** восстанавливаем положение страницы*/
    if ($(this).is("[data-sidebar-closer-mobile]")) {
        $("body").css("top", "");
        $(window).scrollTop(savedScrollTop);
      }
      /** **/

    });

    /** End sidebar toggler for open/close **/
});