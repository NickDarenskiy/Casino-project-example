$(document).ready(function () {
  const DOC = $(document);
  const userLink = $("[data-user-menu]");

  userLink.on("click", function (e) {
    // Проверяем, не был ли клик на аватаре
    if (!$(e.target).is(".user-avatar__img, .user-avatar__img img")) {
      const userMenu = $(this).siblings(".user-menu");
      userLink.toggleClass("active");
      userMenu.toggleClass("opened");
    }
  });

  DOC.on("click", function (e) {
    const userMenu = $(".user-menu");
    if (userMenu.is(":visible")) {
      if (!$(e.target).closest("[data-user-menu], .user-menu").length) {
        userMenu.removeClass("opened");
        userLink.removeClass("active");
      }
      e.stopPropagation();
    }
  });
});
