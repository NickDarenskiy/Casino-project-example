import '../../../css/components/vendor/datepicker.scss';
import '../vendor/datepicker';

function initDatePicker(inputDateElement) {
  const currentYear = new Date().getFullYear();
  const maxDate = new Date();

  const minAge = inputDateElement.data('min-age') ?? 18;
  maxDate.setFullYear(currentYear - minAge);

  const lang = $('html').attr('lang');

  return inputDateElement.datepicker({
    onSelect: function () {
      $(this).change();
    },
    maxDate,
    autoClose: true,
    dateFormat: 'yyyy-mm-dd',
    language: lang,
  });
}

function initDatePickerOnPage() {
  const inputDateElement = $('[data-date]');
  if (inputDateElement.length && inputDateElement.is(':visible')) {
    if (!$('.datepickers-container').length) {
      initDatePicker(inputDateElement);
    }
  }
}

function initDatePickerInModal() {
  const inputDateElement = $('[data-date]');
  return initDatePicker(inputDateElement);
}

$(document).ready(initDatePickerOnPage);

let initialDatePickerCount = 0;

const updatePosition = (datepicker) => {
  if (!datepicker) return null;
  datepicker.update();
}

$('body').on('modal-opened', (_, data) => {
  initialDatePickerCount = $('.datepickers-container').children().length;
  const datepickerInstance = initDatePickerInModal().data('datepicker');
  if (data && datepickerInstance) {
    data.wrap[0].addEventListener('scroll', () => { updatePosition(datepickerInstance)});
  }
});

$('body').on('modal-closed', () => {
  const currentDatePickerCount = $('.datepickers-container').children().length;
  const datepickersToRemove = currentDatePickerCount - initialDatePickerCount;

  for (let i = 0; i < datepickersToRemove; i++) {
    $('.datepickers-container').children(':last-child').remove();
  }
});
