// структура для использования <div id="counter" class="counter"></div>

$(document).ready(function () {
  // Set the date we're counting down to
  var countDownDate = new Date("Jan 5, 2025 15:37:25").getTime();

  // Update the countdown every 1 second
  var x = setInterval(function () {

    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the countdown date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    let counterElement = document.getElementById("counter");
    if (counterElement) {
      counterElement.innerHTML =
        "<span class='counter__cell'>" + days + "<span>d</span>" + "</span>"
        + "<span class='counter__cell'>" + hours + "<span>h</span>" + "</span>"
        + "<span class='counter__cell'>" + minutes + "<span>m</span>" + "</span>";
    }

    // If the countdown is over, write some text
    if (distance < 0) {
      clearInterval(x);
      if (counterElement) {
        counterElement.innerHTML = "EXPIRED";
      }
    }
  }, 1000);
});
