import Swiper from "../vendor/swiper-bundle.min.js";

$(document).ready(function () {
  new Swiper("[data-providers-swiper]", {
    slidesPerView: 5,
    spaceBetween: 12,
    navigation: {
      nextEl: ".providers .swiper-button-next",
      prevEl: ".providers .swiper-button-prev",
    },
    breakpoints: {
      280: {
        slidesPerView: 2,
      },
      480: {
        slidesPerView: 3,
      },
      640: {
        slidesPerView: 4,
        spaceBetween: 24,
      },
      1560: {
        slidesPerView: 6,
        spaceBetween: 24,
      },
    },
  });
});
