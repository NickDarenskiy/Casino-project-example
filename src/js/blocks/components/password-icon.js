$(document).ready(function () {
    $('.switch-icon.icon-eye-blind').on('click', function () {
        $(this).toggleClass('icon-eye icon-eye-blind');

        var passwordField = $(this).siblings('.input').find('input');

        if (passwordField.attr('type') === 'password') {
            passwordField.attr('type', 'text');
        } else {
            passwordField.attr('type', 'password');
        }
    });
});