function promocodeInit() {
  const container = document.querySelector('[data-promocode]');
  if (!container) return;
  const input = container.querySelector('input#promoCode');
  const promoCodeContainer = container.querySelector('.form-promocode__input');

  input.addEventListener('change', (e) => {
    const isChecked = e.target.checked;
    console.log({ promoCodeContainer});
    if (isChecked) {
      promoCodeContainer.classList.add('form-promocode__input--active');
    } else {
      promoCodeContainer.classList.remove('form-promocode__input--active');
    }
  })
}

$(document).ready(promocodeInit)
$('body').on('modal-opened', promocodeInit);