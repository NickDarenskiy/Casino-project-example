$(document).ready(function() {
    (function ($) {
        $('.tab ul.tabs').find('> li:eq(0)').addClass('current');
        $('.tab ul.tabs li').click(function (g) {
            let tab = $(this).closest('.tab'),
                index = $(this).closest('li').index();
            tab.find('ul.tabs > li').removeClass('current');
            $(this).closest('li').addClass('current');

            tab.find('.tab__content').find('div.tabs__item').not('div.tabs__item:eq(' + index + ')').slideUp();
            tab.find('.tab__content').find('div.tabs__item:eq(' + index + ')').slideDown();

            g.preventDefault();

        } );
    })(jQuery);

});
