import Swiper from "../vendor/swiper-bundle.min.js";

function initSwiper() {
  new Swiper("[data-banners-swiper]", {
    slidesPerView: 1,
    spaceBetween: 12,
    grid: {
      rows: 2,
      fill: "row",
    },
    pagination: {
      el: ".swiper-pagination",
      type: "bullets",
      clickable: true,
    },
    breakpoints: {
      768: {
        slidesPerView: 1,
        spaceBetween: 24,
        grid: {
          rows: 2,
          fill: "row",
        },
      },
      1280: {
        slidesPerView: 2,
        spaceBetween: 24,
        grid: {
          rows: 2,
          fill: "row",
        },
      },
    },
  });
}

$(document).ready(() => {
  initSwiper();
});
