import '../vendor/tippy-bundle.min.js';

$(document).ready(function () {
    const getTooltipPlacement = (reference) => {
      let placement = "top";
      if (window.innerWidth > 1279) {
        placement = $(reference).attr('data-tooltip-placement-1280') ?? placement;
      } else if (window.innerWidth > 767) {
        placement = $(reference).attr('data-tooltip-placement-768') ?? placement;
      } else {
        placement = $(reference).attr('data-tooltip-placement-320') ?? placement;
      }
      return placement;
    };
  
    const initializeTooltips = () => {
      const tooltips = tippy('[data-tooltip]', {
        content(reference) {
          return $(reference).find('.tooltip-text').text();
        },
        theme: 'default',
      });
  
      tooltips.forEach(tooltip => {
        tooltip.setProps({placement: getTooltipPlacement(tooltip.reference)});
      });
  
      return tooltips;
    }
  
    let tooltips = initializeTooltips();
  
    window.addEventListener('resize', () => {
      tooltips.forEach(tooltip => {
        tooltip.setProps({placement: getTooltipPlacement(tooltip.reference)});
      });
    });
  
    $(document).on('ajaxSuccess', function () {
      if (tooltips && tooltips.length) {
        tooltips.forEach(tooltip => tooltip.destroy());
      }
  
      tooltips = initializeTooltips();
    });
  });
  