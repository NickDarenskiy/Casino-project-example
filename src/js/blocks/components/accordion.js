$(document).ready(function () {
  $('[data-accordion-opener]').on('click', function() {
    $(this).toggleClass('toggled');
    $('[data-accordion-content]').toggleClass('shown');
  });
});
