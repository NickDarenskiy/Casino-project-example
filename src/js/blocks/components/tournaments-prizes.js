$(document).ready(function () {
  $('[data-select-menu]').on('selectric-change', function () {
    var selectedOption = $(this).children('option:selected').text();
    var $prizesVal = $('.tournaments__prizes-val div');

    $prizesVal.removeClass('active');
    $prizesVal.filter('.' + selectedOption).addClass('active');
  });
});
