import Swiper from '../vendor/swiper-bundle.min.js';

function initSlider(node) {
    const { desktopSlidesPerView, slidesPerView } = node.dataset;

    const sliderOptions = {
        desktopSlidesPerView: desktopSlidesPerView ?? 1,
        slidesPerView: slidesPerView ?? 1,
    }

    new Swiper(node, {
        slidesPerView: sliderOptions.slidesPerView,
        spaceBetween: 35,
        loop: true,
        speed: 1000,
        autoplay: {
            delay: 3000,
            disableOnInteraction: false
        },
        breakpoints: {
            1280: {
                slidesPerView: sliderOptions.desktopSlidesPerView,
                spaceBetween: 24
            }
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        navigation: {
            nextEl: ".slider-main .next",
            prevEl: ".slider-main .prev",
        }
    });
}

$(document).ready(() => {
    const sliders = document.querySelectorAll('[data-main-swiper]');
    if (!sliders.length) return;
    sliders.forEach(initSlider);
});
