import '../../css/pages/games.scss';

import '../blocks/components/swiper-banners.js';

import '../blocks/components/swiper-game-categories.js';

import '../blocks/components/select-tabs';

import '../blocks/components/games.js';
