import '../../css/pages/tournaments-info.scss';
import '../blocks/components/counter';
import '../blocks/components/swiper-games-tournaments.js';
import '../blocks/components/swiper-tournaments.js';
import '../blocks/components/swiper-prizes.js';
import '../blocks/components/tournaments-prizes.js';
import '../blocks/components/tooltips';
import '../blocks/vendor/tippy-bundle.min';
