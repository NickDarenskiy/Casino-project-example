import "../../css/pages/game.scss";
import "../blocks/components/game-full-screen";
import "../blocks/components/dropdown";
import "../blocks/components/select-tabs";
import '../blocks/components/modals';

//SHOW MORE | SHOW LESS

$(document).ready(function () {
  const textElement = $(`[data-game-description]`);
  const showMoreBtn = $("[data-game-description-show]");
  const hideMoreBtn = $("[data-game-description-hide]");

  textElement.find(":last-child").css("margin-bottom", "0");

  const clone = textElement.clone();
  clone.css({
    position: "absolute",
    visibility: "hidden",
    height: "auto",
    width: textElement.width(),
    "word-wrap": "break-word",
  });

  $("body").append(clone);

  const lineHeight = parseFloat(clone.css("line-height"));
  const maxHeight = 2 * lineHeight;

  let actualHeight = clone[0].scrollHeight;

  clone.remove();

  if (actualHeight > maxHeight) {
    textElement.addClass("show-gradient");
    showMoreBtn.show();
    hideMoreBtn.hide();
  } else {
    showMoreBtn.hide();
    hideMoreBtn.hide();
  }

  showMoreBtn.on("click", function () {
    textElement.css("height", "auto");
    textElement.removeClass("show-gradient");
    showMoreBtn.hide();
    hideMoreBtn.show();
  });

  hideMoreBtn.on("click", function () {
    textElement.css("height", `${maxHeight}px`);
    textElement.addClass("show-gradient");
    showMoreBtn.show();
    hideMoreBtn.hide();
  });
});
