import '../../css/pages/main.scss';

import '../blocks/components/swiper-main.js';

import '../blocks/components/swiper-banners.js';

import '../blocks/components/counter.js';

import '../blocks/components/swiper-game-categories.js';

import '../blocks/components/swiper-tournaments.js';

import '../blocks/components/select-tabs';

import '../blocks/components/games.js';
