import '../../css/pages/bonuses.scss';
import '../blocks/components/counter';
import '../blocks/components/select-tabs';
import '../../css/pages/blocked.scss';
import '../../css/pages/bonus-info.scss';

const links = document.querySelectorAll('.button-group__link');
links.forEach(function(link) {
    link.addEventListener('click', function(event) {
        event.preventDefault();

        links.forEach(function(item) {
            item.classList.remove('active');
        });

        this.classList.add('active');
    });
});
