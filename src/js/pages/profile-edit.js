import '../../css/components/f2a-simple.scss'
import '../../css/components/vendor/datepicker.scss'

import '../blocks/vendor/jquery.selectric.min';
import '../blocks/vendor/datepicker';
import '../blocks/components/datepicker';
import '../blocks/components/selectmenu';
import '../blocks/components/password-icon';
import '../blocks/components/tabs-hidden-block';

import '../blocks/components/select-tabs';
