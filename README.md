# Webpack front

## Info
- npm >= `9.7.1`
- node.js >= `14.21.3`

## Install
1) Go to "dev"
2) `npm install`
3) `npm run build`
4) `npm run watch` for developing

## Generate new files
 in terminal "node tools/generate.js"

## Work
- All developing process in `src` directory

## New page
- Create new scss file in "scss/pages/"
- Create new js file in "js/pages/". Add dependencies
- Add dependencies in webpack.config.js (entry)
- Add dependencies in "src/mapping/HtmlWebpackPluginMapping.js" 

## Diferences in nunjucks and twig
Elif is not working - instead use elseif or else with if inside

## Docs 
https://docs.google.com/spreadsheets/d/1RJgSo-ZkQI3IJzJv8UAcLpiH4KP7Lg7gxfkRBTkk1DE/edit?usp=sharing
