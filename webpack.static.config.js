const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const outputPath = './tools/template/static_pages';
const jsFileName = 'unused.js';
const cssFileName = 'styles.css';

module.exports = {
    mode: 'development',
    entry: './src/css/static-page/index.scss',
    output: {
        filename: jsFileName,
        path: path.resolve(__dirname, outputPath)
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: cssFileName
        }),
        new CopyPlugin({
            patterns: [
                {
                    from: `tools/template/static_pages/${cssFileName}`,
                    to: `./blocked/css/${cssFileName}`
                },
                {
                    from: `tools/template/static_pages/${cssFileName}`,
                    to: `./construction/css/${cssFileName}`
                },
                {
                    from: `tools/template/static_pages/${cssFileName}`,
                    to: `./not-available/css/${cssFileName}`
                }
            ]
        }),
        new CleanWebpackPlugin({
            cleanOnceBeforeBuildPatterns: [],
            cleanAfterEveryBuildPatterns: [jsFileName, cssFileName, '*.woff2'],
            protectWebpackAssets: false
            // dry: true // plugin debug mode
        })
    ]
};
