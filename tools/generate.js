const { generateTemplateFiles } = require('generate-template-files');
const { addModalToWebpack, addModalButtonToTemplate } = require('./utilities');

const config = require('../package.json');
const modalOutputPath = '/src/html/modals/';

generateTemplateFiles([
    {
        option: 'Page SCSS',
        defaultCase: '(kebabCase)',
        entry: {
            folderPath: './tools/template/scss/page/',
        },
        stringReplacers: ['pagename'],
        output: {
            path: '../dev/src/css/pages/',
            overwrite: true,
        },
    },
    {
        option: 'Page JS',
        defaultCase: '(kebabCase)',
        entry: {
            folderPath: './tools/template/js/',
        },
        stringReplacers: ['pagename'],
        output: {
            path: '../dev/src/js/pages/',
            overwrite: true,
        },
    },
    {
        option: 'Page HTML',
        defaultCase: '(kebabCase)',
        entry: {
            folderPath: './tools/template/twig/page/',
        },
        stringReplacers: ['pagename'],
        output: {
            path: '../dev/src/html/pages/',
            overwrite: true,
        },
    },
    {
        option: 'Component SCSS',
        defaultCase: '(kebabCase)',
        entry: {
            folderPath: './tools/template/scss/component/',
        },
        stringReplacers: ['componentname'],
        output: {
            path: '../dev/src/css/components/',
            overwrite: true,
        },
    },
    {
        option: 'Component HTML',
        defaultCase: '(pascalCase)',
        entry: {
            folderPath: './tools/template/twig/component/',
        },
        stringReplacers: ['componentname'],
        output: {
            path: '../dev/src/html/components/',
            pathAndFileNameDefaultCase: '(kebabCase)',
            overwrite: true,
        },
    },
    {
        option: 'Component HTML Modal',
        defaultCase: '(pascalCase)',
        entry: {
            folderPath: './tools/template/twig/modal/',
        },
        stringReplacers: ['modalname'],
        output: {
            path: '../dev/src/html/modals/',
            pathAndFileNameDefaultCase: '(kebabCase)',
            overwrite: true,
        },
        onComplete: (results) => {
            try {
              const modalName = results?.output?.files[0].split('//')[1].split('.twig')[0];
              addModalToWebpack(modalName, modalOutputPath);
              addModalButtonToTemplate(modalName);
            } catch (error) {
              throw new Error(error);
            }
          }
    },
]);