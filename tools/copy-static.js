const fsExtra = require('fs-extra');
const path = require('path');
const webpack = require('webpack');
const config = require('../webpack.static.config.js');

function initPath() {
    return {
        sourceDir: path.join(__dirname, 'template/static_pages'),
        destDir: path.join(__dirname, '../../public/static_pages/')
    };
}

function copyStaticPages({ paths }) {
    fsExtra
        .copy(paths.sourceDir, paths.destDir)
        .then(() => {
            console.log('Directories successfully copied!');
        })
        .catch((err) => {
            console.error(err);
        });
}

function runCompliler(compiler) {
    return new Promise((resolve, reject) => {
        compiler.run((err, stats) => {
            if (err) {
                console.error(err.stack || err);
                if (err.details) {
                    console.error(err.details);
                }
                reject(err);
                return;
            }

            const info = stats.toJson();

            if (stats.hasErrors()) {
                info.errors.forEach((error) => console.error(error));
            }

            if (stats.hasWarnings()) {
                info.warnings.forEach((warning) => console.warn(warning));
            }

            console.log(
                stats.toString({
                    chunks: false,
                    colors: true
                })
            );
            resolve(stats);
        });
    });
}

async function init() {
    const compiler = webpack(config);
    const paths = initPath();
    await runCompliler(compiler);
    copyStaticPages({ paths });
}

init();
