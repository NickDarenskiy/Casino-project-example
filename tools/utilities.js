const prettier = require('prettier');
const fs = require('fs');
const readline = require('readline');

const jsPrettierConfig = {
    parser: 'babel',
    singleQuote: true,
    trailingComma: 'none'
};

async function addModalToWebpack(modalName, modalPath) {
    const webpackFilePath = 'src/mapping/HtmlWebpackPluginMapping.js';
    const webpackContent = fs.readFileSync(webpackFilePath, 'utf-8');
    const stringReplace = '// modals\n';

    const getPath = (type) => {
        switch (type) {
            case 'template':
                const pathToTemplate = '.' + modalPath + modalName + '.twig';
                return pathToTemplate;
            case 'filename':
                return `html/modals/${modalName}.html`;
            default:
                throw new Error(
                    'Передан не корректный тип геренируемого шаблона'
                );
        }
    };

    const modalContent = `
      ${stringReplace}
      new HtmlWebpackPlugin({
        chunks: ['modals'],
        template: '${getPath('template')}',
        filename: '${getPath('filename')}'
      }),`;

    const newContent = webpackContent.split(stringReplace).join(modalContent);
    const formattedContent = await prettier.format(
        newContent,
        jsPrettierConfig
    );
    fs.writeFileSync(webpackFilePath, formattedContent, 'utf-8');
}

async function addModalButtonToTemplate(modalName) {
    const templatePath = 'src/html/pages/modals.twig';
    const templateContent = fs.readFileSync(templatePath, 'utf-8');
    const stringReplace = '{# modal buttons #}';

    const buttonContent = `{{ fieldButton({
                    tag: 'a',
                    sizeClass: '1',
                    typeClass: '1',
                    content: '${modalName}',
                    href: '../modals/${modalName}.html',
                    customData: 'data-modal'
                }) }}
                ${stringReplace}`;

    const newContent = templateContent.split(stringReplace).join(buttonContent);
    fs.writeFileSync(templatePath, newContent, 'utf-8');
}

module.exports = { addModalToWebpack, addModalButtonToTemplate };
